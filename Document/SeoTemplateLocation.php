<?php

namespace Nitra\SeoBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\Document
 */
class SeoTemplateLocation
{
    /**
     * @ODM\Id(strategy="NONE")
     * @var string
     */
    private $id;

    /**
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     */
    private $name;

    /**
     * @ODM\EmbedMany(targetDocument="SeoTemplateField")
     */
    private $fields;

    public function __construct()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set id
     *
     * @param custom_id $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return custom_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add field
     *
     * @param Nitra\SeoBundle\Document\SeoTemplateField $field
     */
    public function addField(\Nitra\SeoBundle\Document\SeoTemplateField $field)
    {
        $this->fields[] = $field;
    }

    /**
     * Remove field
     *
     * @param Nitra\SeoBundle\Document\SeoTemplateField $field
     */
    public function removeField(\Nitra\SeoBundle\Document\SeoTemplateField $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return Doctrine\Common\Collections\Collection $fields
     */
    public function getFields()
    {
        return $this->fields;
    }
}