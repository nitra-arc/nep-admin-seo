# SeoBundle

## Описание

данный бандл предназначен для:

* **для управления сео текстами, сео шаблонами**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-seobundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\SeoBundle\NitraSeoBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* **routing.yml**

```yml
nitra_seo:
    resource:    "@NitraSeoBundle/Resources/config/routing.yml"
    prefix:      /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

* **menu.yml**

```yml
seo:
    translateDomain: 'menu'
    route: Nitra_SeoBundle_Seo_list
seoTemplate:
    translateDomain: 'menu'
    route: Nitra_SeoBundle_SeoTemplate_list
```

