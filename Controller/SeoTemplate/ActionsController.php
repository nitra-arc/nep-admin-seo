<?php

namespace Nitra\SeoBundle\Controller\SeoTemplate;

use Admingenerated\NitraSeoBundle\BaseSeoTemplateController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ActionsController extends BaseActionsController
{
    protected $typeDocsSeoTemplate          = '\Nitra\SeoBundle\Form\Type\DocsSeoTemplateType';
    protected $docsSeoTemplateRepository    = '\Nitra\SeoBundle\Document\DocsSeoTemplate';

    /**
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->get('doctrine_mongodb.odm.document_manager');
    }

    /**
     * @return \Symfony\Component\Translation\Translator
     */
    protected function getTranslator()
    {
        return $this->get('translator');
    }

    /**
     * load ligaments for category
     * 
     * @Route("/load-ligaments", name="load-ligaments")
     * 
     * @param Request $request
     * 
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function loadLigamentsAction(Request $request)
    {
        $result = $this->getDefaultsLigaments();

        $regexp = $request->request->get('term')
            ? "/(?=.*" . preg_replace('/[ ]+/', ')(?=.*', trim($request->request->get('term'))) . ")/i"
            : "/.*/";

        $parameters = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Parameter')
            ->hydrate(false)->select('internal_name', 'name', 'alias_en')
            ->field('name')->equals(new \MongoRegex($regexp))
            ->getQuery()->execute();

        foreach ($result as $key => $value) {
            if (!preg_match($regexp, $value['label'])) {
                unset ($result[$key]);
            }
        }

        foreach ($parameters as $id => $parameter) {
            $result[$id] = array(
                'placeholder' => $parameter['alias_en'],
                'label'       => array_key_exists('internal_name', $parameter) && $parameter['internal_name']
                    ? $parameter['internal_name']
                    : $parameter['name'],
            );
        }

        return new JsonResponse($result);
    }

    /**
     * Get default ligaments
     *
     * @return array
     */
    protected function getDefaultsLigaments()
    {
        return array(
            'brand' => array(
                'placeholder' => 'brand',
                'label'       => $this->getTranslator()->trans('seo_template.fields.ligaments.choices.brand', array(), 'NitraSeoBundle'),
            ),
        );
    }

    /**
     * @Template("NitraSeoBundle::docsSeoTemplate.html.twig")
     */
    public function docsSeoTemplateAction(Request $request, $class, $title, $listRoute, $saveRoute)
    {
        $object = $this->getDocsSeo($class);
        $form   = $this->createForm(new $this->typeDocsSeoTemplate(), $object, array(
            'class'     => $class,
            'action'    => $this->generateUrl($saveRoute),
            'attr'      => array(
                'class' => 'admin_form',
            ),
        ));
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDocumentManager()->persist($object);
            $this->getDocumentManager()->flush($object);
            
            $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator'));
            
            if ($request->request->has('save-and-list')) {
                return new RedirectResponse($this->generateUrl($listRoute));
            }
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator'));
        }
        
        return array(
            'title'     => $title,
            'listRoute' => $listRoute,
            'form'      => $form->createView(),
        );
    }
    
    protected function getDocsSeo($class)
    {
        $doc = $this->getDocumentManager()->getRepository($this->docsSeoTemplateRepository)->findOneByClass($class);
        return $doc
            ? $doc
            : new $this->docsSeoTemplateRepository();
    }
}