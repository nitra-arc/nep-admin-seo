<?php

namespace Nitra\SeoBundle\Controller\SeoTemplateLocation;

use Admingenerated\NitraSeoBundle\BaseSeoTemplateLocationController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\JsonResponse;

class ActionsController extends BaseActionsController
{
    public function attemptObjectGetAjaxFields($pk)
    {
        $SeoTemplateLocation = $this->getObject($pk);
        $fields = array();
        foreach($SeoTemplateLocation->getFields() as $field) {
            $fields[$field->getId()] = $field->getName();
        }
        
        return new JsonResponse($fields);
    }
}