<?php

namespace Nitra\SeoBundle\Form\Type\Seo;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmbedSeoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', 'nl_translation', array(
            'fields'        => array(
                'description'   => array(
                    'type'                  => 'ckeditor',
                    'required'              => false,
                    'label'                 => 'seo.fields.text.label',
                    'translation_domain'    => 'NitraSeoBundle',
                ),
                'metaTitle'   => array(
                    'type'                  => 'textarea',
                    'required'              => false,
                    'label'                 => 'seo.fields.meta_title.label',
                    'translation_domain'    => 'NitraSeoBundle',
                ),
                'metaDescription'   => array(
                    'type'                  => 'textarea',
                    'required'              => false,
                    'label'                 => 'seo.fields.meta_desсription.label',
                    'translation_domain'    => 'NitraSeoBundle',
                ),
                'metaKeywords'   => array(
                    'type'                  => 'textarea',
                    'required'              => false,
                    'label'                 => 'seo.fields.meta_keywords.label',
                    'translation_domain'    => 'NitraSeoBundle',
                ),
            ),
        ));
    }

    public function getName()
    {
        return 'embed_seo';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'Nitra\\SeoBundle\\Document\\EmbedSeo',
        ));
    }
}