<?php

namespace Nitra\SeoBundle\Form\Type\SeoTemplateLocation;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmbedSeoTemplateFieldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'text', array(
            'required'          => false,
            'label'             => 'seo_template_field.fields.id.label',
            'help'              => null,
        ));

        $builder->add('name', 'text', array(
            'required'          => false,
            'label'             => 'seo_template_field.fields.name.label',
            'help'              => null,
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'Nitra\\SeoBundle\\Document\\SeoTemplateField',
            'translation_domain'    => 'NitraSeoBundle',
        ));
    }

    public function getName()
    {
        return 'embed_seo_template_field';
    }
}