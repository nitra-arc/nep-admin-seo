<?php

namespace Nitra\SeoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocsSeoTemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('class', 'hidden', array(
            'data'          => $options['class'],
        ));
        $builder->add('translations', 'nl_translation', array(
            'fields'        => array(
                'description'   => array(
                    'type'          => 'ckeditor',
                    'required'      => false,
                    'label'         => 'seo.fields.text.label',
                ),
                'metaTitle'   => array(
                    'type'          => 'textarea',
                    'required'      => false,
                    'label'         => 'seo.fields.meta_title.label',
                ),
                'metaDescription'   => array(
                    'type'          => 'textarea',
                    'required'      => false,
                    'label'         => 'seo.fields.meta_desсription.label',
                ),
                'metaKeywords'   => array(
                    'type'          => 'textarea',
                    'required'      => false,
                    'label'         => 'seo.fields.meta_keywords.label',
                ),
            ),
        ));
        
    }

    public function getName()
    {
        return 'docs_seo_template';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraSeoBundle',
            'data_class'            => 'Nitra\\SeoBundle\\Document\\DocsSeoTemplate',
            'class'                 => null,
        ));
    }
}