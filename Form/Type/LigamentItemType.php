<?php

namespace Nitra\SeoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LigamentItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('data', 'genemu_jqueryselect2_hidden', array(
            'label'     => ' ',
            'configs'   => array(
                'query'                 => 'functionLoadAllowedValues',
            ),
        ));
        $builder->add('order', 'hidden');
    }

    public function getName()
    {
        return 'ligament_item';
    }
}